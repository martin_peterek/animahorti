module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            'css/*.css',
            './index.php',
            './js/main.js'
          ]
        },
        options: {
          watchTask: true,
          proxy: "ah.loc"
        }
      }
    },
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'css/styles.css': 'sass/styles.scss'
        }
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer'),
          // require('cssnano')
        ],
        options: {
          // interrupt: true,
          reload: true,
          livereaload: true,
          // spawn: false
        },
      },
      dist: {
        src: 'css/styles.css'
      }
    },
    watch: {
      scripts: {
        files: ['sass/**/*.scss'],
        tasks: ['sass:dist', 'postcss:dist'],
        options: {
          // interrupt: true,
          reload: true,
          livereaload: true,
          spawn: false
        },
      }
    },
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-browser-sync');

  // Default task(s).
  grunt.registerTask('default', ['browserSync', 'watch']);

};