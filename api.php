<?php
header('Content-Type: application/json');

class A {
    private $conn;
    
    function __construct($servername, $username, $password, $dbname) {
        $this->conn = new mysqli($servername, $username, $password, $dbname);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }
    public function getProjectDetail($projectId){
        $sql = "SELECT * FROM projects where id = $projectId";
        $result = $this->conn->query($sql);
        
        $project;
        if ($result->num_rows > 0) {
            $project = $result->fetch_assoc();
            
            $sql = "SELECT * FROM projects_photos where projectId = $projectId";
            $result = $this->conn->query($sql);
            
            $project['photos'] = array();
            
            while($row = $result->fetch_assoc()) {
                array_push($project['photos'], $row);
            }
            
            
            $project['otherProjects'] = array();
            
            $poradi = $project['poradi'];
            // $sql = "SELECT * FROM projects where poradi < $poradi LIMIT 1";
            // $result = $this->conn->query($sql);
            // while($row = $result->fetch_assoc()) {
            //     array_push($project['otherProjects'], $row);
            // }
            
            $sql = "SELECT id, CZ_nazev, coverSmall FROM projects where poradi != $poradi ORDER BY RAND() LIMIT 4";
            $result = $this->conn->query($sql);
            while($row = $result->fetch_assoc()) {
                array_push($project['otherProjects'], $row);
            }

            $project['nextProjectId'] = null;
            
            $sql = "SELECT id FROM projects where poradi > $poradi ORDER BY poradi ASC LIMIT 1";
            $result = $this->conn->query($sql);
            while($row = $result->fetch_assoc()) {
                $project['nextProjectId'] = $row['id'];
            }

            $project['prevProjectId'] = null;
            
            $sql = "SELECT id FROM projects where poradi < $poradi ORDER BY poradi DESC LIMIT 1";
            $result = $this->conn->query($sql);
            while($row = $result->fetch_assoc()) {
                $project['prevProjectId'] = $row['id'];
            }
            
            
        } else {
            return 'asd';
        }
        
        return $project;
    }
    
    public function getAllProjects(){
        $sql = "SELECT * FROM projects ORDER BY poradi DESC";
        $result = $this->conn->query($sql);
        
        $projects = array();
        
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($projects, $row);
            }
        }
        return $projects;
    }
}

$data = new A("127.0.0.1","animahorti.cz","P98DrCxX","animahorticz2");

if(isset($_GET['projectId'])){
    echo json_encode($data->getProjectDetail($_GET['projectId']));
} else if(isset($_GET['allProjects'])) {
    echo json_encode($data->getAllProjects());
}