<?php

class AH {
    private $conn;
    private $stringCache;
    private $lang = "CZ";
    public $projectOG;
    
    function __construct($servername, $username, $password, $dbname) {
        $this->conn = new mysqli($servername, $username, $password, $dbname);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        
        if(!isset($_COOKIE['lang'])) {
            $this->lang = 'CZ';
        } else {
            $this->lang = $_COOKIE['lang'];
        }
        
        if(isset($_GET['project'])) {
            $this->loadOG($_GET['project']);
        }
    }
    
    function loadOG($projectId) {
        $sql = "SELECT * FROM projects WHERE id = $projectId";
        $result = $this->conn->query($sql);
        
        if ($result->num_rows > 0) {
            $this->projectOG = $result->fetch_assoc();
        }
    }
    
    function setLang($lang){
        $this->lang = $lang;
        setcookie('lang', $lang, time() + (86400 * 30), "/");
    }
    function getLang(){
        return $this->lang;
    }
    
    function getTranslate($key){
        if($this->stringCache){
            foreach ($this->stringCache as $value) {
                if($value['stringKey'] == $key){
                    return $value[ $this->lang . 'Text'];
                }
            }
            return 'Not found key';
        } else {
            $sql = "SELECT * FROM texts";
            $result = $this->conn->query($sql);
            
            if ($result->num_rows > 0) {
                $this->stringCache = array();
                while($row = $result->fetch_assoc()) {
                    array_push($this->stringCache, $row);
                }
                foreach ($this->stringCache as $value) {
                    if($value['stringKey'] == $key){
                        return $value[ $this->lang . 'Text'];
                    }
                }
                return 'Not found key';
            } else {
                return 'Not found key';
            }
        }
    }
    
    public function getProjects(){
        $sql = "SELECT * FROM projects ORDER BY poradi DESC LIMIT 9";
        $result = $this->conn->query($sql);
        
        $projects = array();
        
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($projects, $row);
            }
        }
        return $projects;
    }
}

$data = new AH("127.0.0.1","animahorti.cz","P98DrCxX","animahorticz2");


if(isset($_GET['lang'])){
    $data->setLang($_GET['lang']);
    echo '<script>window.location = "/";</script>';
}
?>

  <!DOCTYPE HTML>

  <html xmlns:og="http://opengraphprotocol.org/schema/">

  <head>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
       google_ad_client: "ca-pub-6958037303631109",
       enable_page_level_ads: true
      });
    </script>
                                                                                                    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <title>Anima Horti</title>

    <!-- assets -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css"></link>
    <link href="css/vendor/lightbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./css/styles.css"></link>
    <!-- END assets -->

    <style>
     .js-open-project,
     #loadMoreProjects
    {
         cursor: pointer;
    }
    </style>

    <?php
if($data->projectOG) {
    ?>
      <meta property="og:title" content="AnimaHorti.cz - <?php echo $data->projectOG['CZ_nazev']; ?>" />
      <meta property="og:type" content="project" />
      <meta property="og:url" content="<?php echo " http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI] "; ?>" />
      <meta property="og:image" content="<?php echo " http://$_SERVER[HTTP_HOST]/assets/img/proj/ " . $data->projectOG['id'] . "/ ". $data->projectOG['coverBig']; ?>" />
      <meta property="og:description" content="<?php echo $data->projectOG['CZ_text']; ?>" />

      <?php
} else {
    ?>
        <meta property="og:title" content="AnimaHorti.cz" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo " http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI] "; ?>" />
        <meta property="og:image" content="<?php echo " http://$_SERVER[HTTP_HOST]/assets/img/proj/1/hodkovicky-uvodni.jpg"; ?>" />
        <meta property="og:description" content="Jsme projekční a realizační firma zabývající se zejména soukromými zahradami ve Zlíně a jeho okolí." />

      <?php
}
?>

  </head>

  <body id="aboutUs">
    <div class="l-mainPage container-fluid">
      <div class="l-mainPage__wrapper">
        <div class="l-mainPage__overlay">
          <div class="mainPage__content container">
            <div class="l-mainPage__texts">
              <h1 class="mainPage__title"><?php echo $data->getTranslate("slider_big"); ?>
</h1>
              <h4 class="mainPage__description"><?php echo $data->getTranslate("slider_p1"); ?></h4>
              <h4 class="mainPage__description"><?php echo $data->getTranslate("slider_p2"); ?></h4>
            </div>
          </div>
        </div>
        <div id="js__slideShow" class="l-mainPage__slideShow l-mainPage__slideShow-xl">
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/anima-horti-uvodni-01.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/anima-horti-uvodni-02.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/anima-horti-uvodni-03.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/anima-horti-uvodni-04.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/anima-horti-uvodni-05.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/anima-horti-uvodni-06.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/anima-horti-uvodni-07.jpg" />
          </div>
        </div>
        <div id="js__slideShow-m" class="l-mainPage__slideShow l-mainPage__slideShow-sm">
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/mobile/anima-horti-uvodni-01.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/mobile/anima-horti-uvodni-02.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/mobile/anima-horti-uvodni-03.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/mobile/anima-horti-uvodni-04.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/mobile/anima-horti-uvodni-05.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/mobile/anima-horti-uvodni-06.jpg" />
          </div>
          <div class="l-mainPage__image">
            <img class="mainPage__image" src="assets/img/slideshow/mobile/anima-horti-uvodni-07.jpg" />
          </div>
        </div>
        <nav class="l-navbar ">
          <div class="navbar container">
            <div id="#aboutUsLink" class="l-navbar__logo">
              <img class="navbar__logo" src="assets/img/logo.svg">
              <div class="navbar__name">
                <div class="navbar__text navbar__text-anima">anima</div>
                <div class="navbar__text navbar__text-horti">horti</div>
              </div>
              <div class="navbar__subtext">
                <?php echo $data->getTranslate("header_logo"); ?>
              </div>
            </div>
            <div class="l-menu">
              <span id="js__menu" class="l-menu__icon">
<img class="menu__icon" src="assets/img/menu.svg">
</span>
              <div id="js__openMenu" class="l-menu__container">
                <span id="#aboutUsLink" class="menu__link"><?php echo $data->getTranslate("menu_about"); ?></span>
                <span id="#projectsLink" class="menu__link"><?php echo $data->getTranslate("menu_projects"); ?></span>
                <span id="#servicesLink" class="menu__link"><?php echo $data->getTranslate("menu_services"); ?></span>
                <span id="#contactLink" class="menu__link"><?php echo $data->getTranslate("menu_contacts"); ?></span>
                <div class="menu__languages">
                  <a href="?lang=CZ" class="menu__language <?php echo $data->getLang() == 'CZ'? 'active':''; ?>">CZ</a>
                  <span class="menu__separator">|</span>
                  <a href="?lang=EN" class="menu__language <?php echo $data->getLang() == 'EN'? 'active':''; ?>">EN</a>
                </div>
                <div class="l-menu__social">
                  <a href="https://www.facebook.com/AnimaHorti/" class="l-menu__facebook" target="_blank">
                    <img class="menu__facebook" src="assets/img/facebook.svg" />
                  </a>
                  <a href="https://www.instagram.com/animahorti/" class="l-menu__facebook" target="_blank">
                    <img class="menu__instagram" src="assets/img/instagram.svg" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>

      <div class="l-projects" id="projects">
        <div class="l-projects__flower l-projects__flower-left">
          <img class="projects__flower" src="assets/img/flower_left.svg" alt="">
        </div>
        <div class="l-projects__flower l-projects__flower-right">
          <img class="projects__flower" src="assets/img/flower_right.svg" alt="">
        </div>
        <div class="container projects">
          <div class="l-projects__header">
            <h2 class="projects__title"><?php echo $data->getTranslate("projects_header"); ?></h2>
          </div>
          <div class="l-project__content" id="projectList">
            <?php
foreach ($data->getProjects() as $key => $value) {
    ?>

              <div class="l-project">
                <div class="grid project__wrapper">
                  <figure class="effect-lexi l-project__image js-open-project" data-project-id="<?php echo $value['id']?>">
                    <img class="project__image" src="assets/img/proj/<?php echo $value['id']?>/<?php echo $value['coverSmall']?>" alt="img12" />
                    <figcaption>
                      <h2 class="project__title"><?php echo $value[$data->getLang() . '_nazev']?></h2>
                      <p>
                        <?php echo $data->getTranslate("projects_detail"); ?>
                      </p>
                    </figcaption>
                  </figure>
                </div>
              </div>
              <?php
}
?>
          </div>
          <div class="l-projects__loadMore">
            <span class="projects__loadMore" id="loadMoreProjects"><?php echo $data->getTranslate("projects_loadmore"); ?></span>
          </div>
        </div>
        <div class="projects__btmShadow"></div>
      </div>

      <div class="l-services" id="services">
        <div class="l-services__bg"></div>
        <div class="services container">
          <h2 class="services__title"><?php echo $data->getTranslate("services_header"); ?></h2>
          <div class="services__description">
            <?php echo $data->getTranslate("services_p6"); ?>
          </div>
          
          <div class="l-services__section">
            <h4 class="services__subtitle"><?php echo $data->getTranslate("services_h4"); ?></h4>
            <div class="services__description">
              <?php echo $data->getTranslate("services_p5"); ?>
            </div>
          </div>
          <div class="l-services__section">
            <h4 class="services__subtitle"><?php echo $data->getTranslate("services_h1"); ?></h4>
            <div class="services__description">
              <?php echo $data->getTranslate("services_p1"); ?>
            </div>
          </div>
          <div class="l-services__section">
            <h4 class="services__subtitle"><?php echo $data->getTranslate("services_h2"); ?></h4>
            <div class="services__description">
              <span class="underline"><?php echo $data->getTranslate("services_u1"); ?></span>
              <?php echo $data->getTranslate("services_p2"); ?>
            </div>
            <div class="services__description">
              <span class="underline"><?php echo $data->getTranslate("services_u2"); ?></span>
              <?php echo $data->getTranslate("services_p3"); ?>
            </div>
          </div>
          <div class="l-services__section">
            <h4 class="services__subtitle"><?php echo $data->getTranslate("services_h3"); ?></h4>
            <div class="services__description">
              <?php echo $data->getTranslate("services_p4"); ?>
            </div>
          </div>
        </div>
        <div class="projects__btmShadow"></div>
      </div>

      <div class="l-contact" id="contact">
        <div class="contact container">
          <div class="row">
            <div class="l-contact__col l-contact__col-0">
              <img class="contact__picture contact__picture-0" src="assets/img/pavel.png" alt="">
            </div>
            <div class="l-contact__col l-contact__col-1">
              <h2 class="contact__title"><?php echo $data->getTranslate("contacts_header"); ?></h2>
              <span class="contact__text bold">Ing. Pavel Jugas</span>
              <span class="contact__text">+420 733 373 544
<br /> info@animahorti.cz
</span>
              <a href="https://www.facebook.com/AnimaHorti/" class="l-contact__facebook" target="_blank">
                <img class="contact__facebook" src="assets/img/facebook.svg" />
              </a>
              <a href="https://www.instagram.com/animahorti/" class="l-contact__facebook" target="_blank">
                <img class="contact__instagram" src="assets/img/instagram.svg" />
              </a>
              <img class="contact__picture contact__picture-1" src="assets/img/pavel.png" alt="">
            </div>
            <div class="l-contact__col l-contact__col-2">
              <span class="contact__text bold">Anima Horti</span>
              <span class="contact__text">Na Kopci 3816
<br /> Zlín 76001</span>
              <span class="contact__text">IČO: 88841553
<br /> DIČ: CZ8505124529</span>
              <a href="" class="l-contact__logo l-logo__logo">
                <img class="logo__logo" src="assets/img/logo_white.svg">
                <div class="logo__name">
                  <div class="logo__text logo__text-anima">anima</div>
                  <div class="logo__text logo__text-horti">horti</div>
                </div>
                <span class="logo__subtext contact__logo_subtext">Zahradní architektura</span>
              </a>
            </div>
            <div class="l-contact__col l-contact__col-3">
              <img class="contact__flower" src="assets/img/footer_flower.svg" />
            </div>
          </div>
        </div>
        <div class="projects__btmShadow"></div>
      </div>

      <div class="l-footer">
        <div class="footer container">
          <div class="footer__text">
            2018 &copy;
            <a href="" class="footer__link">Anima Horti</a>
          </div>
        </div>
      </div>

    </div>


    <div class="modal fade" id="projectModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="l-modal__header">
            <img class="modal__profileImage" id="modalCoverImg" />
            <div class="modal__title" id="modalTitle"></div>
            <button class="modal__close" type="button" data-dismiss="modal" aria-label="Close">
              <img src="assets/img/close.svg" alt="">
            </button>
          </div>
          <div class="l-modal__body">
            <p class="modal__description" id="modalDescription"></p>
            <div class="l-modal__images" id="modalPhotos">
              <img class="modal__image" src="" />
            </div>

            <div class="l-modal__otherProjects">
              <div class="modal__otherProjects_title">
                <?php echo $data->getTranslate("projects_anotherprojects"); ?>
              </div>
              <div class="modal__otherProjects" id="modalOtherProjects"></div>
            </div>
          </div>
          <div class="l-modal__footer">
            <div class="modal__footer">
              <div class="modal__projBtn-prev ">
                <span class="modal__projBtn js-open-project" id="prevProjectBtn">
<img src="assets/img/proj_arrow.svg" class="modal__projArrow modal__projArrow-previous">
<?php echo $data->getTranslate('projects_prev_project'); ?>
</span>
              </div>
              <button type="button" class="projects__loadMore modal__backBtn" data-dismiss="modal">
                <?php echo $data->getTranslate("projects_back"); ?>
              </button>
              <div class="modal__projBtn-next">
                <span class="modal__projBtn  js-open-project" id="nextProjectBtn">
<?php echo $data->getTranslate('projects_next_project'); ?>
<img src="assets/img/proj_arrow.svg" class="modal__projArrow">
</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <input type="hidden" id="detailText" value="<?php echo $data->getTranslate('projects_detail'); ?>">
    <input type="hidden" id="lang" value="<?php echo $data->getLang(); ?>">

    <script type="text/javascript" src="js/vendor/jquery.min.js"></script>
    <script type="text/javascript" src="js/vendor/popper.min.js"></script>
    <script type="text/javascript" src="js/vendor/slick.min.js"></script>
    <script type="text/javascript" src="js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/vendor/lightbox.js"></script>
    <script type="text/javascript" src="js/main.js?v=0"></script>

  </body>

  </html>