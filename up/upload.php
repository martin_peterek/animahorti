<?php

require_once("lib/Tinify/Exception.php");
require_once("lib/Tinify/ResultMeta.php");
require_once("lib/Tinify/Result.php");
require_once("lib/Tinify/Source.php");
require_once("lib/Tinify/Client.php");
require_once("lib/Tinify.php");

\Tinify\setKey("ZqMkK9xOXVWZCtgv8I2h8KuH67pl6-8j");

$projectId = $_POST['projectId'];
$target_dir = "original/" . $projectId . '/';

if(!$_FILES){
    echo 'max file size';
}
for($i=0; $i<count($_FILES['filesToUpload']['name']); $i++) {
    $target_name = basename($_FILES["filesToUpload"]["name"][$i]);
    $target_file = $target_dir . $target_name;
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    // if (isset($_POST["submit"])) {
    //     $check = getimagesize($_FILES["filesToUpload"]["tmp_name"][$i]);
    //     if ($check !== false) {
    //         echo "File is an image - " . $check["mime"] . ".";
    //         $uploadOk = 1;
    //     } else {
    //         echo "File is not an image.";
    //         $uploadOk = 0;
    //     }
    // }
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
    } else {
        if(!is_dir($target_dir)){
            mkdir($target_dir);
        }
        if (move_uploaded_file($_FILES["filesToUpload"]["tmp_name"][$i], $target_file)) {
            if(!is_dir('prepared/' . $projectId)){
                mkdir('prepared/' . $projectId);
            }
            
            $fileName = explode('.', $_FILES["filesToUpload"]["name"][$i]);
            echo "INSERT INTO `projects_photos` (`id`, `projectId`, `title`, `fileName`, `fileExt`) VALUES (NULL, " . $projectId . " , '', '" . $fileName[0] . "', '" . $imageFileType . "'); <BR>";
            $source = \Tinify\fromFile($target_file);
            $full = $source->resize(array(
            "method" => "fit",
            "width" => 1800,
            "height" => 1800
            ));
            $small = $source->resize(array(
            "method" => "fit",
            "width" => 800,
            "height" => 1500
            ));
            $full->toFile("prepared/" . $projectId . '/' . $target_name);
            $small->toFile("prepared/" . $projectId . '/sm-' . $target_name);
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}