$(function () {
 
  $('#js__menu').click(function () {
    $('#js__openMenu').toggleClass('openMenu');
  });

  var currentLocation = window.location;
  if (currentLocation.search) {
    var projectId = currentLocation.search.split('=')[1];
    if (projectId) {
      openModal(projectId);
    }
  }


  $(window).resize(function () {
    if (($(window).width() > 558)) {
      $('#js__openMenu').removeClass('openMenu');
    }
  });

  $('#js__slideShow').slick({
    infinite: true,
    slidesToShow: 1,
    dots: false,
    draggable: false,
    pauseOnFocus: false,
    arrows: false,
    autoplay: true,
    lazyLoad: 'ondemand',
    autoplaySpeed: 4000,
    speed: 700,
    cssEase: 'ease'
  });

  if ($(window).width() < 576) {
    $('#js__slideShow-m').slick({
      infinite: true,
      slidesToShow: 1,
      dots: false,
      draggable: false,
      pauseOnFocus: false,
      arrows: false,
      autoplay: true,
      lazyLoad: 'ondemand',
      autoplaySpeed: 4000,
      speed: 700,
      cssEase: 'ease'
    });
  }

  $('#projectModal').on('hide.bs.modal', function (e) {
    history.pushState(null, '', '/');
  })

  $(document).on("click", '.js-open-project', function (event) {
    var projectId = $(this).data('projectId');
    openModal(projectId);
  });

  function openModal(projectId) {
    var detailText = $('#detailText').val();
    var lang = $('#lang').val();

    history.pushState(null, 'Project A', '?project=' + projectId);

    if ($('#projectModal').hasClass('show')) {
      $("#projectModal").animate({
        scrollTop: 0
      }, 100);
    }
    $.ajax({
        method: "GET",
        url: "api.php",
        data: {
          projectId: projectId
        }
      })
      .done(function (data) {
        var projectImgFolder = 'assets/img/proj/' + projectId + '/';
        if (!$('#projectModal').hasClass('show')) {
          $('#projectModal').modal('show');
        }

        $('#modalDescription').text(data[lang + '_text']);
        $('#modalTitle').text(data[lang + '_nazev']);
        $('#modalCoverImg').attr('src', projectImgFolder + data.coverBig);

        var photos = '';
        data.photos.forEach(p => {
          photos += '<a href="' + projectImgFolder + p.fileName + '.' + p.fileExt + '" data-lightbox="g"><img class="modal__image" alt="image" src="' + projectImgFolder + 'sm-' + p.fileName + '.' + p.fileExt + '" /></a>';
        });
        $('#modalPhotos').html(photos);

        if (data.nextProjectId) {
          $('#prevProjectBtn').data('project-id', data.nextProjectId);
          $('#prevProjectBtn').show();
        } else {
          $('#prevProjectBtn').hide();
        }

        if (data.prevProjectId) {
          $('#nextProjectBtn').data('project-id', data.prevProjectId);
          $('#nextProjectBtn').show();
        } else {
          $('#nextProjectBtn').hide();
        }

        var otherProjects = '';
        data.otherProjects.forEach(p => {
          otherProjects += '<div class="l-project l-project-modal">';
          otherProjects += '  <div class="grid project__wrapper project__wrapper-modal">';
          otherProjects += '    <figure class="effect-lexi l-project__image l-project__image-modal js-open-project" data-project-id="' + p.id + '">';
          otherProjects += '      <img class="project__image" src="assets/img/proj/' + p.id + '/' + p.coverSmall + '" alt="img12" />';
          otherProjects += '      <figcaption>';
          otherProjects += '        <h2 class="project__title project__title-modal">' + p['CZ_nazev'] + '</h2>';
          otherProjects += '        <p class="project__detail-modal">Detail projektu</p>';
          otherProjects += '      </figcaption>';
          otherProjects += '    </figure>';
          otherProjects += '  </div>';
          otherProjects += '</div>';
        });
        $('#modalOtherProjects').html(otherProjects);
      });
  }



  $(document).on("click", '#loadMoreProjects', function (event) {
    event.preventDefault();
    var detailText = $('#detailText').val();
    $.ajax({
        method: "GET",
        url: "api.php",
        data: {
          allProjects: ''
        }
      })
      .done(function (data) {

        var project = '';
        data.forEach(p => {
          project += '<div class="l-project">';
          project += '  <div class="grid project__wrapper">';
          project += '  <figure class="effect-lexi l-project__image js-open-project" data-project-id="' + p.id + '">';
          project += '      <img class="project__image" src="assets/img/proj/' + p.id + '/' + p.coverSmall + '" alt="img12" />';
          project += '         <figcaption>';
          project += '      <h2 class="project__title">' + p.CZ_nazev + '</h2>';
          project += '        <p>';
          project += detailText;
          project += '     </p>';
          project += '    </figcaption>';
          project += '   </figure>';
          project += ' </div>';
          project += '</div>';
        });

        $('#projectList').html(project);
        $('#loadMoreProjects').hide();
      });
  });

  $('span[id*="#"], div[id*="#"]').click(function () {
    var link = $(this).attr('id').replace('#', '');
    var tempLink = '#' + link.replace('Link', '');
    if (tempLink === '#aboutUs' || tempLink === '#projects' || tempLink === '#services' || tempLink === '#contact') {
      if ($('#js__openMenu').hasClass('openMenu')) {
        $('#js__openMenu').removeClass('openMenu')
      }
      $('html, body').animate({
        scrollTop: $(tempLink).offset().top
      }, 500);
      setTimeout(function () {
        $('.l-navbar').removeClass('hideMenu');
      }, 550);
    } else {
      return;
    }
  });


  $(window).scroll(function () {
    if ($('.l-navbar').offset().top > 10) {
      $('.l-navbar').addClass('scrolled');
    } else {
      $('.l-navbar').removeClass('scrolled');
    }
  });


  var iScrollPos = 0;

  $(window).scroll(function () {
    var iCurScrollPos = $(this).scrollTop();
    if (iCurScrollPos > iScrollPos) {
      $('.l-navbar').addClass('hideMenu');
    } else {
      $('.l-navbar').removeClass('hideMenu')
    }
    iScrollPos = iCurScrollPos;
  });



});